--
-- --------------------------------------------------------
--
-- Structure de la table Categorie
--

CREATE TABLE Categorie (
  Code char(2) NOT NULL ,
  Libelle varchar(25) ,
  salaireMini decimal(10,2),
  CaisseDeRetraite varchar(5),
  PrimeResultat integer ,
  PRIMARY KEY (Code)
) ;

--
-- Contenu de la table Categorie
--

INSERT INTO Categorie (Code, Libelle, salaireMini, CaisseDeRetraite, PrimeResultat) VALUES ('C1', 'Cadre moyen', 1900, 'AGIRC', 1);
INSERT INTO Categorie (Code, Libelle, salaireMini, CaisseDeRetraite, PrimeResultat) VALUES ('C2', 'Cadre sup�rieur', 2500, 'AGIRC', 1);
INSERT INTO Categorie (Code, Libelle, salaireMini, CaisseDeRetraite, PrimeResultat) VALUES ('E1', 'Employ� niveau 1', 1250, 'ARRCO', 0);
INSERT INTO Categorie (Code, Libelle, salaireMini, CaisseDeRetraite, PrimeResultat) VALUES ('E2', 'Employ� niveau 2', 1500, 'ARRCO', 0);

-- --------------------------------------------------------

--
-- Structure de la table Formation
--

CREATE TABLE Formation (
  Code char(3) NOT NULL,
  Nom varchar(50) DEFAULT NULL,
  DateDebut date DEFAULT NULL,
  Nbrejours integer DEFAULT NULL,
  CoutJourForm decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (Code)
) ;

--
-- Contenu de la table Formation
--

INSERT INTO Formation (Code, Nom, DateDebut, Nbrejours, CoutJourForm) VALUES ('F01', 'Visual Basic d�butant', '2006-02-05', 4, 800);
INSERT INTO Formation (Code, Nom, DateDebut, Nbrejours, CoutJourForm) VALUES ('F02', 'Visual Basic initi�', '2006-05-20', 5, 800);
INSERT INTO Formation (Code, Nom, DateDebut, Nbrejours, CoutJourForm) VALUES ('F03', 'Administration d''un r�seau local sous Linux', '2006-09-14', 6, 900);
INSERT INTO Formation (Code, Nom, DateDebut, Nbrejours, CoutJourForm) VALUES ('F04', 'D�velopper en PHP', '2006-05-10', 4, 800);
INSERT INTO Formation (Code, Nom, DateDebut, Nbrejours, CoutJourForm) VALUES ('F05', 'La r�alisation d''un projet informatique', '2006-05-17',3, 1300);
INSERT INTO Formation (Code, Nom, DateDebut, Nbrejours, CoutJourForm) VALUES ('F06', 'SQL-Server', '2006-02-25', 4, 1000);
INSERT INTO Formation (Code, Nom, DateDebut, Nbrejours, CoutJourForm) VALUES ('F07', 'Ciel paye', '2006-03-14', 3, 700);
INSERT INTO Formation (Code, Nom, DateDebut, Nbrejours, CoutJourForm) VALUES ('F08', 'Anglais d�butant', '2006-02-25', 8, 400);
INSERT INTO Formation (Code, Nom, DateDebut, Nbrejours, CoutJourForm) VALUES ('F09', 'Anglais avanc�', '2006-04-05', 3, 1100);
INSERT INTO Formation (Code, Nom, DateDebut, Nbrejours, CoutJourForm) VALUES ('F10', 'Relations client�le', '2006-05-08', 3, 800);
INSERT INTO Formation (Code, Nom, DateDebut, Nbrejours, CoutJourForm) VALUES ('F11', 'La fiscalit� des PME', '2006-10-17', 2, 1000);
INSERT INTO Formation (Code, Nom, DateDebut, Nbrejours, CoutJourForm) VALUES ('F12', 'Fixer des objectifs � votre force de vente', '2006-10-20', 1, 850);
INSERT INTO Formation (Code, Nom, DateDebut, Nbrejours, CoutJourForm) VALUES ('F13', 'Optimiser son portefeuille clients', '2006-01-02', 3, 600);
INSERT INTO Formation (Code, Nom, DateDebut, Nbrejours, CoutJourForm) VALUES ('F14', 'Utiliser un logiciel de gestion de client�le', '2006-02-09', 5, 900);

-- --------------------------------------------------------

--
-- Structure de la table Salarie
--

CREATE TABLE Salarie (
  Code char(3) NOT NULL ,
  Nom varchar(7) DEFAULT NULL,
  Prenom varchar(8) DEFAULT NULL,
  DateNaiss date DEFAULT NULL,
  DateEmbauche date DEFAULT NULL,
  Fonction varchar(23) DEFAULT NULL,
  TauxHoraire decimal(10,2) DEFAULT NULL,
  situationFamiliale varchar(20) DEFAULT NULL,
  NbrEnfants integer DEFAULT NULL,
  NumCat char(2) DEFAULT NULL,
  CodeServ integer DEFAULT NULL,
  PRIMARY KEY (Code)
) ;

--
-- Contenu de la table Salarie
--

INSERT INTO Salarie (Code, Nom, Prenom, DateNaiss, DateEmbauche, Fonction, TauxHoraire, situationFamiliale, NbrEnfants, NumCat, CodeServ) VALUES ('S01', 'RETAIS', 'Claude', '1962-03-31', '2000-09-01', 'Chef de projet', 19.5, 'Marié', 3, 'C2', 1);
INSERT INTO Salarie (Code, Nom, Prenom, DateNaiss, DateEmbauche, Fonction, TauxHoraire, situationFamiliale, NbrEnfants, NumCat, CodeServ) VALUES ('S02', 'BERNARD', 'Céline', '1972-08-14', '2000-09-01', 'Directrice commercial', 19.5, 'Mariée', 2, 'C2', 3);
INSERT INTO Salarie (Code, Nom, Prenom, DateNaiss, DateEmbauche, Fonction, TauxHoraire, situationFamiliale, NbrEnfants, NumCat, CodeServ) VALUES ('S03', 'RETAIS', 'Jérôme', '1968-09-14', '2000-09-01', 'Ingénieur informatique', 14, 'Divorcé', 4, 'C1', 1);
INSERT INTO Salarie (Code, Nom, Prenom, DateNaiss, DateEmbauche, Fonction, TauxHoraire, situationFamiliale, NbrEnfants, NumCat, CodeServ) VALUES ('S04', 'DOMARD', 'Pierre', '1960-06-14', '2000-09-01', 'Directeur général', 22.5, 'Marié', 3, 'C2', 2);
INSERT INTO Salarie (Code, Nom, Prenom, DateNaiss, DateEmbauche, Fonction, TauxHoraire, situationFamiliale, NbrEnfants, NumCat, CodeServ) VALUES ('S05', 'LALOIS', 'Régis', '1967-07-25', '2000-09-01', 'Chef comptable', 17.3, 'Célibataire', 0, 'C2', 2);
INSERT INTO Salarie (Code, Nom, Prenom, DateNaiss, DateEmbauche, Fonction, TauxHoraire, situationFamiliale, NbrEnfants, NumCat, CodeServ) VALUES ('S06', 'DUPONT', 'Henri', '1968-11-15', '2000-09-01', 'Développeur', 11.6, 'Marié', 2, 'E2', 1);
INSERT INTO Salarie (Code, Nom, Prenom, DateNaiss, DateEmbauche, Fonction, TauxHoraire, situationFamiliale, NbrEnfants, NumCat, CodeServ) VALUES ('S07', 'AJAVAR', 'Karima', '1981-11-25', '2001-08-01', 'Standardiste', 8.2, 'Célibataire', 1, 'E1', 2);
INSERT INTO Salarie (Code, Nom, Prenom, DateNaiss, DateEmbauche, Fonction, TauxHoraire, situationFamiliale, NbrEnfants, NumCat, CodeServ) VALUES ('S08', 'FALERT', 'Maud', '1980-08-25', '2002-01-01', 'Développeur', 10.3, 'Mariée', 2, 'E2', 1);
INSERT INTO Salarie (Code, Nom, Prenom, DateNaiss, DateEmbauche, Fonction, TauxHoraire, situationFamiliale, NbrEnfants, NumCat, CodeServ) VALUES ('S09', 'MALE', 'Emilie', '1980-01-14', '2002-02-01', 'Assistant Comptable', 8.7, 'Divorcée', 2, 'E1', 2);
INSERT INTO Salarie (Code, Nom, Prenom, DateNaiss, DateEmbauche, Fonction, TauxHoraire, situationFamiliale, NbrEnfants, NumCat, CodeServ) VALUES ('S10', 'MOUDA', 'Mustapha', '1958-01-14', '2002-05-01', 'Ingénieur Commercial', 12.6, 'Marié', 3, 'C1', 3);
INSERT INTO Salarie (Code, Nom, Prenom, DateNaiss, DateEmbauche, Fonction, TauxHoraire, situationFamiliale, NbrEnfants, NumCat, CodeServ) VALUES ('S11', 'AJAVAR', 'Médhi', '1978-02-14', '2002-06-01', 'Développeur', 10, 'Marié', 4, 'E2', 1);
INSERT INTO Salarie (Code, Nom, Prenom, DateNaiss, DateEmbauche, Fonction, TauxHoraire, situationFamiliale, NbrEnfants, NumCat, CodeServ) VALUES ('S12', 'WANG', 'Vinthan', '1980-07-22', '2002-06-01', 'Développeur', 10, 'Marié', 1, 'E2', 1);
INSERT INTO Salarie (Code, Nom, Prenom, DateNaiss, DateEmbauche, Fonction, TauxHoraire, situationFamiliale, NbrEnfants, NumCat, CodeServ) VALUES ('S13', 'SAVOY', 'Marilyne', '1980-07-16', '2003-01-01', 'Secrétaire Commerciale', 8.7, 'Mariée', 3, 'E1', 3);
INSERT INTO Salarie (Code, Nom, Prenom, DateNaiss, DateEmbauche, Fonction, TauxHoraire, situationFamiliale, NbrEnfants, NumCat, CodeServ) VALUES ('S14', 'ESTOUDE', 'Sophie', '1976-08-17', '2003-01-01', 'Assistante de direction', 10, 'Divorcée', 2, 'E2', 2);
INSERT INTO Salarie (Code, Nom, Prenom, DateNaiss, DateEmbauche, Fonction, TauxHoraire, situationFamiliale, NbrEnfants, NumCat, CodeServ) VALUES ('S15', 'PETIT', 'Sylvie', '1979-05-28', '2003-01-01', 'Technico Commercial', 10, 'Mariée', 0, 'E2', 3);
INSERT INTO Salarie (Code, Nom, Prenom, DateNaiss, DateEmbauche, Fonction, TauxHoraire, situationFamiliale, NbrEnfants, NumCat, CodeServ) VALUES ('S16', 'GIRARDO', 'Pablo', '1980-08-14', '2003-05-01', 'Administrateur réseau', 9.1, 'Célibataire', 1, 'E1', 1);
INSERT INTO Salarie (Code, Nom, Prenom, DateNaiss, DateEmbauche, Fonction, TauxHoraire, situationFamiliale, NbrEnfants, NumCat, CodeServ) VALUES ('S17', 'ZOARD', 'Farid', '1983-07-23', '2003-10-01', 'Développeur-Stagiaire', 8.5, 'Célibataire', 0, 'E1', 1);

-- --------------------------------------------------------

--
-- Structure de la table Service
--

CREATE TABLE Service (
  Code integer NOT NULL,
  Designation varchar(13) DEFAULT NULL,
  Email varchar(27) DEFAULT NULL,
  Tel char(10) DEFAULT NULL,
  PRIMARY KEY (Code)
) ;

--
-- Contenu de la table Service
--

INSERT INTO Service (Code, Designation, Email, Tel) VALUES (1, 'Informatique', 'Inf-logihome@logihome.com', '0169983212');
INSERT INTO Service (Code, Designation, Email, Tel) VALUES (2, 'Adminstration', 'Admin-logihome@logihome.com', '0169983210');
INSERT INTO Service (Code, Designation, Email, Tel) VALUES (3, 'Commercial', 'Com-Logihome@logihome.com', '0169983215');
INSERT INTO Service (Code, Designation, Email, Tel) VALUES (4, 'Comptable', ' ', '0169983218');

-- --------------------------------------------------------

--
-- Structure de la table Suivre
--

CREATE TABLE Suivre (
  CodeSal char(3) NOT NULL ,
  CodeForm char(3) NOT NULL,
  PRIMARY KEY (CodeSal,CodeForm)
) ;

--
-- Contenu de la table Suivre
--

INSERT INTO Suivre (CodeSal, CodeForm) VALUES ('S01', 'F04');
INSERT INTO Suivre (CodeSal, CodeForm) VALUES ('S02', 'F10');
INSERT INTO Suivre (CodeSal, CodeForm) VALUES ('S02', 'F12');
INSERT INTO Suivre (CodeSal, CodeForm) VALUES ('S03', 'F05');
INSERT INTO Suivre (CodeSal, CodeForm) VALUES ('S05', 'F11');
INSERT INTO Suivre (CodeSal, CodeForm) VALUES ('S06', 'F01');
INSERT INTO Suivre (CodeSal, CodeForm) VALUES ('S06', 'F02');
INSERT INTO Suivre (CodeSal, CodeForm) VALUES ('S07', 'F08');
INSERT INTO Suivre (CodeSal, CodeForm) VALUES ('S09', 'F07');
INSERT INTO Suivre (CodeSal, CodeForm) VALUES ('S10', 'F08');
INSERT INTO Suivre (CodeSal, CodeForm) VALUES ('S11', 'F04');
INSERT INTO Suivre (CodeSal, CodeForm) VALUES ('S13', 'F14');
INSERT INTO Suivre (CodeSal, CodeForm) VALUES ('S14', 'F09');
INSERT INTO Suivre (CodeSal, CodeForm) VALUES ('S15', 'F13');
INSERT INTO Suivre (CodeSal, CodeForm) VALUES ('S16', 'F03');
INSERT INTO Suivre (CodeSal, CodeForm) VALUES ('S17', 'F06');

-- --------------------------------------------------------

--
-- Contraintes
--

--
-- Contraintes pour la table Salarie
--
ALTER TABLE Salarie ADD CONSTRAINT Salarie_ibfk_1 FOREIGN KEY (NumCat) REFERENCES Categorie (Code);
ALTER TABLE Salarie ADD CONSTRAINT Salarie_ibfk_2 FOREIGN KEY (CodeServ) REFERENCES Service (Code);

--
-- Contraintes pour la table Suivre
--

ALTER TABLE Suivre  ADD CONSTRAINT Suivre_ibfk_2 FOREIGN KEY (CodeForm) REFERENCES Formation (Code);
ALTER TABLE Suivre  ADD CONSTRAINT Suivre_ibfk_1 FOREIGN KEY (CodeSal) REFERENCES Salarie (Code);
