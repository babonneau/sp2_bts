package modele.metier;

import java.sql.Date;


public class Salarie {
    //Déclaration des variables
    private String id;
    private String nom;
    private String prenom;
    private Date dateNaiss;
    private Date dateEmbauche;
    private String fonction;
    private double tauxHoraire;
    private String situationFamiliale;
    private int nbrEnfants;
    private Categorie categorie;
    private Service service;
    
    //génération du constructeur
    public Salarie(String id, String nom, String prenom, Date dateNaiss, Date dateEmbauche, String fonction, double tauxHoraire, String situationFamiliale, int nbrEnfants, Categorie categorie, Service service) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaiss = dateNaiss;
        this.dateEmbauche = dateEmbauche;
        this.fonction = fonction;
        this.tauxHoraire = tauxHoraire;
        this.situationFamiliale = situationFamiliale;
        this.nbrEnfants = nbrEnfants;
        this.categorie = categorie;
        this.service = service;
    }

    public Salarie(Salarie oneById) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    //générations des accesseurs et muttateurs
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDateNaiss() {
        return dateNaiss;
    }

    public void setDateNaiss(Date dateNaiss) {
        this.dateNaiss = dateNaiss;
    }

    public Date getDateEmbauche() {
        return dateEmbauche;
    }

    public void setDateEmbauche(Date dateEmbauche) {
        this.dateEmbauche = dateEmbauche;
    }

    public String getFonction() {
        return fonction;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    public double getTauxHoraire() {
        return tauxHoraire;
    }

    public void setTauxHoraire(double tauxHoraire) {
        this.tauxHoraire = tauxHoraire;
    }

    public String getSituationFamiliale() {
        return situationFamiliale;
    }

    public void setSituationFamiliale(String situationFamiliale) {
        this.situationFamiliale = situationFamiliale;
    }

    public int getNbrEnfants() {
        return nbrEnfants;
    }

    public void setNbrEnfants(int nbrEnfants) {
        this.nbrEnfants = nbrEnfants;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    
    //génération du ToString
    @Override
    public String toString() {
        return nom +"  " + prenom;
    }
    
    public String toStringEtat() {
        return "salarie{" + "id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", dateNaiss=" + dateNaiss + ", dateEmbauche=" + dateEmbauche + ", fonction=" + fonction + ", tauxHoraire=" + tauxHoraire + ", situationFamiliale=" + situationFamiliale + ", nbrEnfants=" + nbrEnfants + ", categorie=" + categorie + ", service=" + service + '}';
    }

    
}
