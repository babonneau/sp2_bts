package modele.metier;

public class Service {
    //déclaration des varaibles
    private int code;
    private String designation;
    private String email;
    private String telephone;

    //génération du constructeur
    public Service(int code, String designation, String email, String telephone) {
        this.code = code;
        this.designation = designation;
        this.email = email;
        this.telephone = telephone;
    }

    //génération des assesseurs et muttateurs
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    //génération du ToSring
    
    public String toStringEtat() {
        return "service{" + "code=" + code + ", designation=" + designation + ", email=" + email + ", telephone=" + telephone + '}';
    }
    @Override
    public String toString() {
        return designation;
    }
}
