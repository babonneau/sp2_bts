package modele.metier;

public class Categorie {
    //déclaration des variables
    private String code;
    private String libelle;
    private double salaireMini;
    private String caisseRetraite;
    private int prime;

    //génération du constructeur
    public Categorie(String code, String libelle, double salaireMini, String caisseRetraite, int prime) {
        this.code = code;
        this.libelle = libelle;
        this.salaireMini = salaireMini;
        this.caisseRetraite = caisseRetraite;
        this.prime = prime;
    }

    //génération des accesseurs et muttateur
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public double getSalaireMini() {
        return salaireMini;
    }

    public void setSalaireMini(double salaireMini) {
        this.salaireMini = salaireMini;
    }

    public String getCaisseRetraite() {
        return caisseRetraite;
    }

    public void setCaisseRetraite(String caisseRetraite) {
        this.caisseRetraite = caisseRetraite;
    }

    public int getPrime() {
        return prime;
    }

    public void setPrime(int prime) {
        this.prime = prime;
    }

    //génération du toString
    @Override
    public String toString() {
        return "categorie{" + "code=" + code + ", libelle=" + libelle + ", salaireMini=" + salaireMini + ", caisseRetraite=" + caisseRetraite + ", prime=" + prime + '}';
    }
    
}
