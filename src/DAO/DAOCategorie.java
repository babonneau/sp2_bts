package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modele.metier.Categorie;
import test.TestJdbc;

public class DAOCategorie {
    public static Categorie  getOneById(String id) throws SQLException{
        Categorie  uneCategorie = null;
        Connection cnx = TestJdbc.connecter();
        // préparer la requête
        String requete = "SELECT * FROM Categorie WHERE code= ?";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        pstmt.setString(1, id);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            String libelle = rs.getString("libelle");
            double salaireMini = rs.getDouble("salairemini");
            String caisseRetraite = rs.getString("caissederetraite");
            int prime = rs.getInt("primeresultat");
            uneCategorie = new Categorie (id, libelle, salaireMini, caisseRetraite, prime);
        }
        return uneCategorie;
    }
     public static List<Categorie > getAll() throws SQLException {
        List<Categorie > lesCategorie = new ArrayList<Categorie >();
        Categorie  uneCategorie;
        Connection cnx = TestJdbc.connecter();
        // préparer la requête
        String requete = "SELECT * FROM Categorie ";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            String id = rs.getString("code");
            String libelle = rs.getString("libelle");
            double salaireMini = rs.getDouble("salairemini");
            String caisseRetraite = rs.getString("caissederetraite");
            int prime = rs.getInt("primeresultat");
            uneCategorie = new Categorie (id, libelle, salaireMini, caisseRetraite, prime);
            lesCategorie.add(uneCategorie);
        }
        return lesCategorie;
    }
}
