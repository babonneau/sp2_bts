package DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modele.metier.*;
import java.util.ArrayList;
import java.util.List;
import modele.metier.Categorie;
import modele.metier.Salarie;
import test.TestJdbc;

public class DAOSalarie {
    public static Salarie  getOneById(String id) throws SQLException{
        Salarie  unSalarie = null;
        Connection cnx = TestJdbc.connecter();
        // préparer la requête
        String requete = "SELECT * FROM Salarie WHERE code= ?";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        pstmt.setString(1, id);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
             String nom = rs.getString("nom");
            String prenom = rs.getString("prenom");
            Date datenaiss = rs.getDate("datenaiss");
            Date dateembauche = rs.getDate("dateembauche");
            String fonction = rs.getString("fonction");
            double tauxhoraire = rs.getDouble("tauxhoraire");
            String situationfamiliale = rs.getString("situationfamiliale");
            int nbrenfants = rs.getInt("nbrenfants");
            String idcat=rs.getString("numcat");
            Categorie numcat = DAOCategorie.getOneById(idcat);
            int idserv =rs.getInt("codeserv");
            Service codeserv = DAOService.getOneById(idserv);
            unSalarie = new Salarie (id, nom, prenom, datenaiss, dateembauche, fonction, tauxhoraire, situationfamiliale, nbrenfants, numcat, codeserv);
        }
        return unSalarie;
    }
     public static List<Salarie > getAll() throws SQLException {
        List<Salarie > lesSalaries = new ArrayList<Salarie >();
        Salarie  unSalarie;
        Connection cnx = TestJdbc.connecter();
        // préparer la requête
        String requete = "SELECT * FROM Salarie ";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            String id = rs.getString("code");
            String nom = rs.getString("nom");
            String prenom = rs.getString("prenom");
            Date datenaiss = rs.getDate("datenaiss");
            Date dateembauche = rs.getDate("dateembauche");
            String fonction = rs.getString("fonction");
            double tauxhoraire = rs.getDouble("tauxhoraire");
            String situationfamiliale = rs.getString("situationfamiliale");
            int nbrenfants = rs.getInt("nbrenfants");
            String idcat=rs.getString("numcat");
            Categorie numcat = DAOCategorie.getOneById(idcat);
            int idserv =rs.getInt("codeserv");
            Service codeserv = DAOService.getOneById(idserv);
            unSalarie = new Salarie (id, nom, prenom, datenaiss, dateembauche, fonction, tauxhoraire, situationfamiliale, nbrenfants, numcat, codeserv);
            lesSalaries.add(unSalarie);
        }
        return lesSalaries;
    }
     
     public static List<Salarie > getAllByService(int code) throws SQLException {
        List<Salarie > lesSalaries = new ArrayList<Salarie >();
        Salarie  unSalarie;
        Connection cnx = TestJdbc.connecter();
        // préparer la requête
        String requete = "SELECT * FROM Salarie WHERE codeserv = ? ORDER BY nom ASC ";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        pstmt.setInt(1, code);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            String id = rs.getString("code");
            String nom = rs.getString("nom");
            String prenom = rs.getString("prenom");
            Date datenaiss = rs.getDate("datenaiss");
            Date dateembauche = rs.getDate("dateembauche");
            String fonction = rs.getString("fonction");
            double tauxhoraire = rs.getDouble("tauxhoraire");
            String situationfamiliale = rs.getString("situationfamiliale");
            int nbrenfants = rs.getInt("nbrenfants");
            String idcat=rs.getString("numcat");
            Categorie numcat = DAOCategorie.getOneById(idcat);
            int idserv =rs.getInt("codeserv");
            Service codeserv = DAOService.getOneById(idserv);
            unSalarie = new Salarie (id, nom, prenom, datenaiss, dateembauche, fonction, tauxhoraire, situationfamiliale, nbrenfants, numcat, codeserv);
            lesSalaries.add(unSalarie);
        }
        return lesSalaries;
    }
}
