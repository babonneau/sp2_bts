package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modele.metier.Service;
import test.TestJdbc;

public class DAOService {
    public static Service getOneById(int id) throws SQLException{
        Service unservice = null;
        Connection cnx = TestJdbc.connecter();
        // préparer la requête
        String requete = "SELECT * FROM Service WHERE code= ?";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        pstmt.setInt(1, id);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            int code = rs.getInt("code");
            String designation = rs.getString("designation");
            String email = rs.getString("email");
            String telephone = rs.getString("tel");
            unservice = new Service(id, designation,email,telephone);
        }
        return unservice;
    }
     public static List<Service> getAll() throws SQLException {
        List<Service> lesServices = new ArrayList<Service>();
        Service unService;
        Connection cnx = TestJdbc.connecter();
        // préparer la requête
        String requete = "SELECT * FROM Service ORDER BY designation ASC";
        PreparedStatement pstmt = cnx.prepareStatement(requete);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            int id = rs.getInt("code");
            String designation = rs.getString("designation");
            String email = rs.getString("email");
            String telephone = rs.getString("tel");
            unService = new Service(id, designation,email,telephone);
            lesServices.add(unService);
        }
        return lesServices;
    }
     public static String [] designationService(List<Service> lesServices){
         int dim = lesServices.size();
         String [] tab = new String[dim];
         for(int i=0;i<dim;i++){
             tab[1]=lesServices.get(i).getDesignation();
         }
         return tab;
     }
}
