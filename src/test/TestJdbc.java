package test;

import java.sql.Driver;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author btssio
 */
public class TestJdbc {

    private static String url = "jdbc:mysql://172.15.6.240:3306/tbabonneau_infoware";
    private static String utilBDD = "tbabonneau";
    private static String mdpBDD = "nnoxl01";
    private static Connection cnx;

    public static Connection connecter() throws SQLException {
        if (cnx == null) {
            cnx = DriverManager.getConnection(url, utilBDD, mdpBDD);
        }
        return cnx;
    }

    public static void initialiser(String url, String login, String mdp) {
        TestJdbc.url = url;
        utilBDD = login;
        mdpBDD = mdp;
    }
}
